<?php

namespace Drupal\projects_audit\Commands;

use Consolidation\AnnotatedCommand\CommandResult;
use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\update\UpdateManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Commands for projects_audit.
 */
class ProjectsAuditCommands extends DrushCommands {

  /**
   * Get a list of unsupported packages.
   *
   * Will return a non-zero exit code if there
   * are any unsupported packages and print the
   * package information.
   *
   * @usage projects_audit:unsupported
   *   Get a list of unsupported packages.
   *
   * @command projects_audit:unsupported
   */
  public function unsupported($options = ['format' => 'table', 'fields' => '']): CommandResult {
    $available = update_get_available(TRUE);
    $projects = update_calculate_project_data($available);
    $unsupported = [];
    foreach ($projects as $moduleName => $moduleValue) {
      if ($moduleValue['status'] === UpdateManagerInterface::NOT_SUPPORTED) {
        $unsupported[$moduleName] = $moduleValue;
      }
    }

    $table = [];
    foreach ($unsupported as $unsupportedModuleName => $unsupportedModule) {
      $table[$unsupportedModuleName] = [
        'Title' => $unsupportedModule['title'],
        'Type' => $unsupportedModule['project_type'],
        'Label' => isset($unsupportedModule['extra'][0]['label']) ? $unsupportedModule['extra'][0]['label']->__toString() : '',
        'Reason' => isset($unsupportedModule['extra'][0]['data']) ? $unsupportedModule['extra'][0]['data']->__toString() : '',
      ];
    }
    if (!empty($table)) {
      return CommandResult::dataWithExitCode(new RowsOfFields($table), 1);
    }

    return CommandResult::exitCode(0);
  }

}
